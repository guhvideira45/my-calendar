<?php
ob_start();

require __DIR__ . "/vendor/autoload.php";

/**
 * BOOTSTRAP
 */

use Source\Core\Session;
use CoffeeCode\Router\Router;

$session = new Session();
$route = new Router(url(), ":");
$route->namespace("Source\App");
$route->group("/");

//LOGIN
$route->get("/", "Login:root");
$route->get("/login", "Login:login");
$route->post("/login", "Login:login");
$route->get("/logoff", "Login:logoff");

//EVENT
$route->group("/events");
$route->get("/", "Events:home");
$route->get("/view/{id}", "Events:view");

$route->post("/save", "Events:save");
$route->post("/delete", "Events:delete");

//PROFILES
$route->group("/profiles");
$route->get("/", "Profiles:home");
$route->get("/{search}/{page}", "Profiles:home");
$route->get("/create", "Profiles:create");
$route->get("/view/{id}", "Profiles:view");

$route->post("/filter", "Profiles:home");
$route->post("/create", "Profiles:save");
$route->post("/update", "Profiles:save");
$route->post("/delete", "Profiles:delete");

//USERS
$route->group("/users");
$route->get("/", "Users:home");
$route->get("/{search}/{page}", "Users:home");
$route->get("/create", "Users:create");
$route->get("/view/{id}", "Users:view");
$route->get("/password", "Users:password");

$route->post("/filter", "Users:home");
$route->post("/create", "Users:save");
$route->post("/update", "Users:save");
$route->post("/delete", "Users:delete");
$route->post("/password", "Users:password");

/** ERROR ROUTER */
$route->group("/");
$route->namespace("Source\App")->group("/ooops");
$route->get("/{errcode}", "Error:error");

/** ROUTER */
$route->dispatch();

/** ERROR REDIRECT */
if ($route->error()) {
    $route->redirect("/ooops/{$route->error()}");
}

ob_end_flush();
