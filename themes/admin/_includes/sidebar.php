<?php 
if(!isset($app)) $app = "";
if(!isset($group)) $group = "";
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<a href="<?= url("/events"); ?>" class="brand-link text-center">
		<span class="brand-text font-weight-light"><?= CONF_SITE_NAME; ?></span>
	</a>
	<div class="sidebar">
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item">
					<a href="<?= url("/events"); ?>" class="nav-link <?= ($app == "events" ? "active" : ""); ?>">
						<i class="nav-icon fas fa-calendar-alt"></i>
						<p>Agenda</p>
					</a>
				</li>
				<?php if(hasPermission(["user_view", "user_create", "profile_view", "profile_create"])): ?>
					<li class="nav-item <?= ($group == "settings" ? "menu-open" : ""); ?>">
						<a href="#" class="nav-link <?= ($group == "settings" ? "active" : ""); ?>">
							<i class="nav-icon fas fa-cogs"></i>
							<p>
								Configurações
								<i class="fas fa-angle-left right"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<?php if(hasPermission(["user_view", "user_create"])): ?>
								<li class="nav-item">
									<a href="<?= url("/users") ?>" class="nav-link <?= ($app == "users" ? "active" : ""); ?>">
										<i class="fas fa-users nav-icon"></i>
										<p>Usuários</p>
									</a>
								</li>
							<?php endif; ?>
							<?php if(hasPermission(["profile_view", "profile_create"])): ?>
								<li class="nav-item">
									<a href="<?= url("/profiles"); ?>" class="nav-link <?= ($app == "profiles" ? "active" : ""); ?>">
										<i class="fas fa-user-shield nav-icon"></i>
										<p>Perfis de acesso</p>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</li>
				<?php endif; ?>
			</ul>
		</nav>
	</div>
</aside>