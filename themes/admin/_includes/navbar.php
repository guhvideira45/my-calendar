<nav class="main-header navbar navbar-expand navbar-dark">
	<ul class="navbar-nav ml-auto">
		<li class="nav-item">
			<a class="nav-link" href="<?= url("/users/password") ?>" title="Alterar senha">
				<i class="fas fa-unlock-alt"></i> Alterar senha
			</a>
    </li>
		<li class="nav-item">
			<a class="nav-link" href="<?= url("/logoff") ?>" title="Sair">
				<i class="fas fa-sign-out-alt"></i> Sair
			</a>
    </li>
	</ul>
</nav>