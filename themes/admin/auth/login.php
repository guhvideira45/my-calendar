<?php $v->layout("_auth"); ?>

<div class="login-box">
  <div class="login-logo">
    <a href="<?= url("/login"); ?>"><b>My</b> Hair</a>
  </div>
  
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Fazer Login</p>
      <div class="ajax_response"><?= flash(); ?></div>

      <form class="ajax_off" name="login" action="<?= url("/login"); ?>" method="post">
        <?= csrf_input(); ?>
        <div class="input-group mb-3">
          <input name="email" type="email" class="form-control" placeholder="Informe seu e-mail:" value="<?= ($cookie ?? null) ?>" required/>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input name="password" type="password" class="form-control" placeholder="Informe sua senha:" required/>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button class="btn btn-primary btn-block">Entrar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>