<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= $head; ?>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<?= $v->section("styles"); ?>
		<link rel="stylesheet" href="<?= theme("/assets/style.css", CONF_VIEW_THEME_ADMIN); ?>" />
		<link rel="icon" type="image/png" href="<?= theme("/assets/images/favicon.png", CONF_VIEW_THEME_ADMIN); ?>" />
	</head>
	<body class="sidebar-mini layout-fixed"> <!-- PARA USAR O DARK-MODE BASTA ADICIONAR A CLASSE dark-mode NO BODY -->
		<div class="wrapper">
			<!-- <div class="preloader flex-column justify-content-center align-items-center">
				<img class="animation__wobble" src="<?= theme("/assets/dist/img/AdminLTELogo.png", CONF_VIEW_THEME_ADMIN); ?>" alt="AdminLTELogo" height="60" width="60">
			</div> -->
			<div class="ajax_load">
				<div class="ajax_load_box">
					<div class="ajax_load_box_circle"></div>
					<p class="ajax_load_box_title">Aguarde, carregando...</p>
				</div>
			</div>

			<!-- Navbar -->
			<?php $v->insert("_includes/navbar.php"); ?>

			<!-- Main Sidebar -->
			<?php $v->insert("_includes/sidebar.php"); ?>

			<!-- Content -->
			<div class="content-wrapper">
				<?= $v->section("content"); ?>
			</div>

			<!-- Main Footer -->
			<?php $v->insert("_includes/footer.php"); ?>
		</div>

		<script src="<?= theme("/assets/scripts.js", CONF_VIEW_THEME_ADMIN); ?>"></script>
		<script>
			$(".select2").select2();
		</script>
		<?= $v->section("scripts"); ?>
	</body>
</html>