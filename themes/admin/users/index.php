<?php $v->layout("_theme"); ?>

<?php if(hasPermission(["user_view", "user_create"])): ?>
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-<?= (hasPermission(["user_create"]) ? "8": "12"); ?>">
					<h1 class="m-0"><i class="fas fa-sm fa-user"></i> Usuários</h1>
				</div>
				<?php if(hasPermission(["user_create"])): ?>
					<div class="col-sm-4">
						<a href="<?= url("/users/create"); ?>" class="btn btn-primary font-weight-bold float-right" ><i class="fas fa-plus-circle"></i> Incluir</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<?php $v->insert("_includes/messageFlash.php"); ?>
			<div class="card card-solid">
				<div class="card-body <?= (!empty($users) ? "pb-1" : "") ?>">
					<div class="row">
						<?php if(!empty($users)): ?>
							<?php foreach ($users as $user): ?>
								<?php $userPhoto = ($user->photo() ? image($user->photo, 300, 300) : theme("/assets/images/avatar.jpg", CONF_VIEW_THEME_ADMIN)); ?>
								<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
									<div class="card bg-light d-flex flex-fill">
										<div class="card-header text-muted border-bottom-0"></div>
										<div class="card-body pt-0">
											<div class="row">
												<div class="col-7">
													<h2 class="lead"><b><?= str_limit_words($user->fullName(), 2); ?></b></h2>
													<ul class="ml-0 mb-0 fa-ul text-muted">
														<li class="small"><?= str_limit_chars($user->email, 25); ?></li>
														<li class="small">Desde: <?= date_fmt($user->created_at, "d/m/y \à\s H\hi"); ?></li>
													</ul>
												</div>
												<div class="col-5 text-center">
													<img src="<?= $userPhoto; ?>" alt="user-avatar" class="img-circle img-fluid">
												</div>
											</div>
										</div>
										<div class="card-footer">
											<?php if(hasPermission(["user_delete"])): ?>
												<a href="#" class="btn font-weight-bold text-danger" data-post="<?= url("/users/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o usuário e todos os dados relacionados a ele? Essa ação não pode ser feita!" data-id="<?= $user->id; ?>"><i class="fas fa-exclamation-triangle"></i> Excluir</a>
											<?php endif; ?>
											<a href="<?= url("/users/view/{$user->id}"); ?>" class="btn btn-sm btn-primary float-right" title="Gerenciar"><i class="fas fa-pencil-alt"></i> Gerenciar</a>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						<?php else: ?>
							<div class="col-sm-12 text-center">
								<h4>Não existem usuários cadastrados até o momento<?php if(hasPermission(["user_create"])): ?>, <a href="<?= url("/users/create"); ?>">clique aqui</a> para incluir um novo<?php endif; ?>!</h4>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<?php if($paginator): ?>
					<div class="card-footer">
						<nav>
							<?= $paginator; ?>
						</nav>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>