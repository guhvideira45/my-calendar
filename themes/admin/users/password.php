<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-8">
				<h1 class="m-0"><i class="fas fa-sm fa-user"></i> <?= $user->fullName(); ?></h1>
			</div>
			<div class="col-sm-4">
				<a href="<?= url("/users"); ?>" class="btn font-weight-bold float-right" ><i class="fas fa-arrow-left"></i> Voltar</a>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="container-fluid">
		<?php $v->insert("_includes/messageFlash.php"); ?>
		<form action="<?= url("/users/password"); ?>" method="post">
			<!--ACTION SPOOFING-->
			<input type="hidden" name="action" value="password" />
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>*Nova senha:</label>
										<input type="password" name="password" class="form-control" placeholder="Por favor insira a nova senha..." required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>*Confirme a senha:</label>
										<input type="password" name="password_confirm" class="form-control" placeholder="Confirme a nova senha..." required>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button class="btn btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Atualizar</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</section>