<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-8">
				<?php if(!$user): ?>
					<h1 class="m-0"><i class="fas fa-sm fa-plus-circle"></i> Incluir</h1>
				<?php else: ?>
					<h1 class="m-0"><i class="fas fa-sm fa-user"></i> <?= $user->fullName(); ?></h1>
				<?php endif; ?>
			</div>
			<div class="col-sm-4">
				<a href="<?= url("/users"); ?>" class="btn font-weight-bold float-right" ><i class="fas fa-arrow-left"></i> Voltar</a>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="container-fluid">
		<?php $v->insert("_includes/messageFlash.php"); ?>
		<?php if(!$user): ?>
			<div class="row">
				<div class="col-md-12">
				<div class="card">
						<form action="<?= url("/users/create"); ?>" method="post">
							<!--ACTION SPOOFING-->
							<input type="hidden" name="action" value="create" />
							<div class="card-body">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Nome:</label>
											<input type="text" name="first_name" class="form-control" placeholder="Primeiro nome" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Sobrenome:</label>
											<input type="text" name="last_name" class="form-control" placeholder="Último nome" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Nascimento:</label>
											<input type="text" name="datebirth" class="form-control mask-date" placeholder="dd/mm/yyyy">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*CPF:</label>
											<input type="text" name="document" class="form-control mask-cpf" placeholder="CPF do usuário" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*E-mail:</label>
											<input type="email" name="email" class="form-control" placeholder="Melhor e-mail" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Senha:</label>
											<input type="password" name="password" class="form-control" placeholder="Senha de acesso" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Perfil de acesso:</label>
											<select class="form-control" name="profile" required>
												<option value="" selected disabled>Selecione uma opção</option>
												<?php if(!empty($profiles)): ?>
													<?php foreach ($profiles as $profile): ?>
														<option value="<?= $profile->id ?>"><?= $profile->name; ?></option>
													<?php endforeach; ?>
												<?php endif; ?>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Status:</label>
											<select class="form-control" name="status">
												<option value="active">Ativo</option>
												<option value="inactive">Inativo</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button class="btn btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Criar usuário</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		<?php else: ?>
			<?php $userPhoto = ($user->photo() ? image($user->photo, 300, 300) : theme("/assets/images/avatar.jpg", CONF_VIEW_THEME_ADMIN)); ?>
			<?php if(hasPermission(["user_update"])): ?>
			<form action="<?= url("/users/update"); ?>" method="post">
				<!--ACTION SPOOFING-->
				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="id" value="<?= $user->id; ?>" />
				<?php endif; ?>
				<div class="row">
					<div class="col-md-3">
						<div class="card card-primary card-outline">
							<div class="card-body box-profile">
								<div class="text-center">
									<img class="profile-user-img img-fluid img-circle" src="<?= $userPhoto; ?>" alt="<?= $user->fullName(); ?>">
								</div>
								<h3 class="profile-username text-center"><?= $user->fullName(); ?></h3>
								<p class="text-muted text-center"><?= $user->profile()->name; ?></p>
								<?php if(hasPermission(["user_update"])): ?>
									<div class="form-group">
										<label for="photo">Foto: (600x600px)</label>
										<div class="input-group">
											<div class="custom-file">
												<input type="file" class="custom-file-input" id="photo" name="photo">
												<label class="custom-file-label" for="photo">Escolher arquivo</label>
											</div>
										</div>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Nome:</label>
											<input type="text" name="first_name" class="form-control" value="<?= $user->first_name; ?>" placeholder="Primeiro nome" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Sobrenome:</label>
											<input type="text" name="last_name" class="form-control" value="<?= $user->last_name; ?>" placeholder="Último nome" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Nascimento:</label>
											<input type="text" name="datebirth" class="form-control mask-date" value="<?= (!empty($user->datebirth) ? date_fmt($user->datebirth, "d/m/Y") : ""); ?>" placeholder="dd/mm/yyyy">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*CPF:</label>
											<input type="text" name="document" class="form-control mask-cpf" value="<?= $user->document; ?>" placeholder="CPF do usuário" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*E-mail:</label>
											<input type="email" name="email" class="form-control" value="<?= $user->email; ?>" placeholder="Melhor e-mail" required>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>Alterar Senha:</label>
											<input type="password" name="password" class="form-control" placeholder="Nova senha de acesso">
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Perfil de acesso:</label>
											<select class="form-control" name="profile" required>
												<option value="" selected disabled>Selecione uma opção</option>
												<?php
													$profile = $user->profile_id;
													$select = function ($value) use ($profile) {
														return ($profile == $value ? "selected" : "");
													};
												?>
												<?php if(!empty($profiles)): ?>
													<?php foreach ($profiles as $profile): ?>
														<option <?= $select($profile->id); ?> value="<?= $profile->id ?>"><?= $profile->name; ?></option>
													<?php endforeach; ?>
												<?php endif; ?>
											</select>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label>*Status:</label>
											<select class="form-control" name="status">
												<?php
													$status = $user->status;
													$select = function ($value) use ($status) {
														return ($status == $value ? "selected" : "");
													};
												?>
												<option <?= $select("active"); ?> value="active">Ativo</option>
												<option <?= $select("inactive"); ?> value="inactive">Inativo</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<?php if(hasPermission(["user_update", "user_delete"])): ?>
								<div class="card-footer">
									<?php if(hasPermission(["user_delete"])): ?>
										<a href="#" class="btn font-weight-bold text-danger" data-post="<?= url("/users/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o usuário e todos os dados relacionados a ele? Essa ação não pode ser feita!" data-id="<?= $user->id; ?>"><i class="fas fa-exclamation-triangle"></i> Excluir Usuário</a>
									<?php endif; ?>
									<?php if(hasPermission(["user_update"])): ?>
										<button class="btn btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Atualizar</button>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			<?php if(hasPermission(["user_update"])): ?>	
			</form>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</section>