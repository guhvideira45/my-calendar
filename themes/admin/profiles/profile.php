<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-8">
				<?php if(!$profile): ?>
					<h1 class="m-0"><i class="fas fa-sm fa-plus-circle"></i> Incluir</h1>
				<?php else: ?>
					<h1 class="m-0"><i class="fas fa-sm fa-user-shield"></i> <?= $profile->name; ?></h1>
				<?php endif; ?>
			</div>
			<div class="col-sm-4">
				<a href="<?= url("/profiles"); ?>" class="btn font-weight-bold float-right" ><i class="fas fa-arrow-left"></i> Voltar</a>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="container-fluid">
		<?php if(!$profile): ?>
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<form action="<?= url("/profiles/create"); ?>" method="post">
							<input type="hidden" name="action" value="create" />
							<div class="card-body">
								<?php $v->insert("_includes/messageFlash.php"); ?>
								<div class="row">
									<div class="col-sm-12 col-md-8">
										<div class="form-group">
											<label>*Nome:</label>
											<input type="text" name="name" class="form-control" placeholder="Nome do perfil de acesso" required>
										</div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="form-group">
											<label>Administrador:</label>
											<select class="form-control" name="admin" id="isAdmin">
												<option value="true">Sim</option>
												<option value="false" selected>Não</option>
											</select>
										</div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="form-group">
											<label>Status:</label>
											<select class="form-control" name="status">
												<option value="active" selected>Ativo</option>
												<option value="inactive">Inativo</option>
											</select>
										</div>
									</div>
								</div>
								<hr>
								<h4>Permissões</h4>
								<div class="row">
									<div class="col-sm-12 col-md-3">
										<div class="nav flex-column nav-tabs nav-pills">
											<a class="nav-link active" href="#permissions-events" data-toggle="pill"><i class="fas fa-sm fa-calendar-alt"></i> Agenda</a>
											<a class="nav-link" href="#permissions-users" data-toggle="pill"><i class="fas fa-sm fa-users"></i> Usuários</a>
											<a class="nav-link" href="#permissions-profiles" data-toggle="pill"><i class="fas fa-sm fa-user-shield"></i> Perfis de acesso</a>
										</div>
										<hr class="d-md-none">
									</div>
									<div class="col-sm-12 col-md-9">
										<div class="tab-content">
											<div class="tab-pane fade active show" id="permissions-events">
												<h5>Eventos da Agenda</h5>
												<hr>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="eventCreate" value="event_create">
														<label class="custom-control-label" for="eventCreate">Incluir evento na agenda</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="eventUpdate" value="event_update">
														<label class="custom-control-label" for="eventUpdate">Atualizar evento da agenda</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="eventDelete" value="event_delete">
														<label class="custom-control-label" for="eventDelete">Excluir evento da agenda</label>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="permissions-users">
												<h5>Usuários</h5>
												<hr>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userView" value="user_view">
														<label class="custom-control-label" for="userView">Visualizar cadastro de usuários</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userCreate" value="user_create">
														<label class="custom-control-label" for="userCreate">Incluir cadastro de usuários</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userUpdate" value="user_update">
														<label class="custom-control-label" for="userUpdate">Atualizar cadastro de usuários</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userDelete" value="user_delete">
														<label class="custom-control-label" for="userDelete">Excluir cadastro de usuários</label>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="permissions-profiles">
												<h5>Perfis de Acesso</h5>
												<hr>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileView" value="profile_view">
														<label class="custom-control-label" for="profileView">Visualizar cadastro de perfis de acesso</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileCreate" value="profile_create">
														<label class="custom-control-label" for="profileCreate">Incluir cadastro de perfis de acesso</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileUpdate" value="profile_update">
														<label class="custom-control-label" for="profileUpdate">Atualizar cadastro de perfis de acesso</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileDelete" value="profile_delete">
														<label class="custom-control-label" for="profileDelete">Excluir cadastro de perfis de acesso</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<div class="row">
									<div class="col-sm-12 col-md-2 offset-md-10">
										<button class="btn btn-block btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Salvar</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		<?php else: ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<?php if(hasPermission(["profile_update"])): ?>
						<form action="<?= url("/profiles/update"); ?>" method="post">
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="id" value="<?= $profile->id; ?>">
							<?php endif; ?>
							<div class="card-body">
								<?php $v->insert("_includes/messageFlash.php"); ?>
								<div class="row">
									<div class="col-sm-12 col-md-8">
										<div class="form-group">
											<label>*Nome:</label>
											<input type="text" name="name" class="form-control" placeholder="Nome do perfil de acesso" value="<?= ($profile->name ?? ""); ?>" required>
										</div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="form-group">
											<label>Administrador:</label>
											<select class="form-control" name="admin" id="isAdmin">
												<option value="true" <?= (($profile->permissions ?? "") == "admin" ? "selected" : "") ?>>Sim</option>
												<option value="false" <?= (($profile->permissions ?? "") != "admin" ? "selected" : "") ?>>Não</option>
											</select>
										</div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="form-group">
											<label>Status:</label>
											<select class="form-control" name="status">
												<option value="active" <?= (($profile->status ?? "") == "active" ? "selected" : "") ?>>Ativo</option>
												<option value="inactive" <?= (($profile->status ?? "") != "active" ? "selected" : "") ?>>Inativo</option>
											</select>
										</div>
									</div>
								</div>
								<hr>
								<h4>Permissões</h4>
								<div class="row">
									<?php $permissions = (!empty($profile->permissions) ? explode(";", $profile->permissions) : []); ?>
									<div class="col-sm-12 col-md-3">
										<div class="nav flex-column nav-tabs nav-pills">
											<a class="nav-link active" href="#permissions-events" data-toggle="pill"><i class="fas fa-sm fa-calendar-alt"></i> Agenda</a>
											<a class="nav-link" href="#permissions-users" data-toggle="pill"><i class="fas fa-sm fa-users"></i> Usuários</a>
											<a class="nav-link" href="#permissions-profiles" data-toggle="pill"><i class="fas fa-sm fa-user-shield"></i> Perfis de acesso</a>
										</div>
										<hr class="d-md-none">
									</div>
									<div class="col-sm-12 col-md-9">
										<div class="tab-content">
											<div class="tab-pane fade active show" id="permissions-events">
												<h5>Eventos da Agenda</h5>
												<hr>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="eventCreate" value="event_create" <?= (in_array("event_create", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="eventCreate">Incluir evento na agenda</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="eventUpdate" value="event_update" <?= (in_array("event_update", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="eventUpdate">Atualizar evento da agenda</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="eventDelete" value="event_delete" <?= (in_array("event_delete", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="eventDelete">Excluir evento da agenda</label>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="permissions-users">
												<h5>Usuários</h5>
												<hr>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userView" value="user_view" <?= (in_array("user_view", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="userView">Visualizar cadastro de usuários</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userCreate" value="user_create" <?= (in_array("user_create", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="userCreate">Incluir cadastro de usuários</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userUpdate" value="user_update" <?= (in_array("user_update", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="userUpdate">Atualizar cadastro de usuários</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="userDelete" value="user_delete" <?= (in_array("user_delete", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="userDelete">Excluir cadastro de usuários</label>
													</div>
												</div>
											</div>
											<div class="tab-pane fade" id="permissions-profiles">
												<h5>Perfis de Acesso</h5>
												<hr>
												<div class="form-group">
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileView" value="profile_view" <?= (in_array("profile_view", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="profileView">Visualizar cadastro de perfis de acesso</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileCreate" value="profile_create" <?= (in_array("profile_create", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="profileCreate">Incluir cadastro de perfis de acesso</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileUpdate" value="profile_update" <?= (in_array("profile_update", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="profileUpdate">Atualizar cadastro de perfis de acesso</label>
													</div>
													<div class="custom-control custom-checkbox">
														<input type="checkbox" class="custom-control-input" name="permissions[]" id="profileDelete" value="profile_delete" <?= (in_array("profile_delete", $permissions) ? "checked" : ""); ?>>
														<label class="custom-control-label" for="profileDelete">Excluir cadastro de perfis de acesso</label>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php if(hasPermission(["profile_update", "profile_delete"])): ?>
								<div class="card-footer">
									<div class="row">
										<?php if(hasPermission(["profile_delete"])): ?>
											<div class="col-sm-12 col-md-3">
												<a href="#" class="btn font-weight-bold text-danger" data-post="<?= url("/profiles/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o perfil de acesso? Essa ação não pode ser feita!" data-id="<?= $profile->id; ?>"><i class="fas fa-exclamation-triangle"></i> Excluir cadastro</a>
											</div>
										<?php endif; ?>
										<?php if(hasPermission(["profile_update"])): ?>
											<div class="col-sm-12 col-md-2 offset-md-<?= (hasPermission(["profile_delete"]) ? "7" : "10"); ?>">
												<button class="btn btn-block btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Atualizar</button>
											</div>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
						<?php if(hasPermission(["profile_update"])): ?>
						</form>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>

<?php $v->start("scripts"); ?>
<script>
	<?php if($profile): ?>
		$(function() {
			$("#isAdmin").change();
		});
	<?php endif; ?>

	$("#isAdmin").on("change", function(e) {
		let admin = $("#isAdmin").val();

		if(admin == "true") {
			$.each($("input[type='checkbox'][name='permissions[]']"), function(i, key) {
				$(this).prop("disabled", true);
			});
		} else {
			$.each($("input[type='checkbox'][name='permissions[]']"), function(i, key) {
				$(this).prop("disabled", false);
			});
		}
	});
</script>
<?php $v->end(); ?>