<?php $v->layout("_theme"); ?>

<?php if(hasPermission(["profile_view", "profile_create"])): ?>
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-<?= (hasPermission(["profile_create"]) ? "8": "12"); ?>">
					<h1 class="m-0"><i class="fas fa-sm fa-user-shield"></i> Perfis de acesso</h1>
				</div>
				<?php if(hasPermission(["profile_create"])): ?>
					<div class="col-sm-4">
						<a href="<?= url("/profiles/create"); ?>" class="btn btn-primary font-weight-bold float-right" ><i class="fas fa-plus-circle"></i> Incluir</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section class="content">
		<div class="container-fluid">
			<?php $v->insert("_includes/messageFlash.php"); ?>
			<div class="card card-solid">
				<?php if(!empty($profiles)): ?>
					<div class="card-header">
						<h3 class="card-title">Perfis de acesso</h3>
						<div class="card-tools">
							<form method="post" action="<?= url("/profiles/filter") ?>" enctype="multipart/form-data">
								<div class="input-group input-group-sm" style="width: 300px;">
									<input type="text" name="s" class="form-control float-right" placeholder="Pesquisar..." <?= (!empty($search) ? "value=\"{$search}\"" : ""); ?>>
									<div class="input-group-append">
										<button type="submit" class="btn btn-default">
											<i class="fas fa-search"></i>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="card-body table-responsive p-0">	
						<table class="table text-nowrap">
							<thead>
								<tr>
									<th>#</th>
									<th>Nome</th>
									<th>Usuários</th>
									<th>Admin</th>
									<th>Status</th>
									<th>Data Inclusão</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($profiles as $profile): ?>
								<tr>
									<td><?= $profile->id; ?></td>
									<td><?= $profile->name; ?></td>
									<td><?= $profile->userCount(); ?></td>
									<td><span class="badge bg-<?= ($profile->permissions == "admin" ? "success" : "secondary"); ?>"><?= ($profile->permissions == "admin" ? "Sim" : "Não") ?></span></td>
									<td><span class="badge bg-<?= ($profile->status == "active" ? "success" : "secondary"); ?>"><?= ($profile->status == "active" ? "Ativo" : "Inativo") ?></span></td>
									<td><?= date_fmt($profile->created_at); ?></td>
									<td>
										<a href="<?= url("/profiles/view/{$profile->id}"); ?>" class="btn btn-primary" title="Alterar cadastro"><i class="fa fa-pencil-alt"></i></a>
										<?php if(hasPermission(["profile_delete"])): ?>
											<a href="#" class="btn btn-danger" title="Excluir cadastro" data-post="<?= url("/profiles/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o perfil de acesso? Essa ação não pode ser feita!" data-id="<?= $profile->id; ?>"><i class="fa fa-trash"></i></button>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<?php if($paginator): ?>
						<div class="card-footer">
							<nav>
								<?= $paginator; ?>
							</nav>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<div class="card-body">
						<div class="row">
							<div class="col-sm-12 text-center">
								<h4>Não existem perfis de acesso cadastrados até o momento<?php if(hasPermission(["profile_create"])): ?>, <a href="<?= url("/profiles/create"); ?>">clique aqui</a> para incluir um novo<?php endif; ?>!</h4>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>