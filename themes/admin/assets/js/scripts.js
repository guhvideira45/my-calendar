// JQUERY INIT

$(function () {
  var effecttime = 200;

  //DATA SET
  $("[data-post]").click(function (e) {
    e.preventDefault();

    var clicked = $(this);
    var data = clicked.data();
    var load = $(".ajax_load");

    if (data.confirm) {
      var deleteConfirm = confirm(data.confirm);
      if (!deleteConfirm) {
        return;
      }
    }

    $.ajax({
      url: data.post,
      type: "POST",
      data: data,
      dataType: "json",
      beforeSend: function () {
        load.fadeIn(200).css("display", "flex");
      },
      success: function (response) {
        //redirect
        if (response.redirect) {
          window.location.href = response.redirect;
        } else {
          load.fadeOut(200);
        }

        //reload
        if (response.reload) {
          window.location.reload();
        } else {
          load.fadeOut(200);
        }

        //message
        if (response.message) {
          ajaxMessage(response.message);
        }
      },
      error: function () {
        showToastr("Desculpe mas não foi possível processar sua requisição...", "error");
        load.fadeOut();
      }
    });
  });

  //FORMS
  $("form:not('.ajax_off')").submit(function (e) {
    e.preventDefault();

    var form = $(this);
    var load = $(".ajax_load");

    form.ajaxSubmit({
      url: form.attr("action"),
      type: "POST",
      dataType: "json",
      beforeSend: function () {
        load.fadeIn(200).css("display", "flex");
      },
      uploadProgress: function (event, position, total, completed) {
        var loaded = completed;
        var load_title = $(".ajax_load_box_title");
        load_title.text("Enviando (" + loaded + "%)");

        if (completed >= 100) {
          load_title.text("Aguarde, carregando...");
        }
      },
      success: function (response) {
        //redirect
        if (response.redirect) {
          window.location.href = response.redirect;
        } else {
          form.find("input[type='file']").val(null);
          load.fadeOut(200);
        }

        //reload
        if (response.reload) {
          window.location.reload();
        } else {
          load.fadeOut(200);
        }

        //message
        if (response.message) {
          ajaxMessage(response.message);
        }
      },
      complete: function () {
        if (form.data("reset") === true) {
          form.trigger("reset");
        }
      },
      error: function () {
        showToastr("Desculpe mas não foi possível processar sua requisição...", "error");
        load.fadeOut();
      }
    });
  });

  //SLIDE
  $("[data-slidedown]").click(function (e) {
    let clicked = $(this);
    let slidedown = clicked.data("slidedown");
    $(slidedown).slideDown(effecttime);
  });

  $("[data-slideup]").click(function (e) {
    let clicked = $(this);
    let slideup = clicked.data("slideup");
    $(slideup).slideUp(effecttime);
  });

  //TOOGLE CLASS
  $("[data-toggleclass]").click(function (e) {
    let clicked = $(this);
    let toggle = clicked.data("toggleclass");
    clicked.toggleClass(toggle);
  });

  // MAKS
  $(".mask-card").mask('0000  0000  0000  0000', {reverse: true}); 
  $(".mask-plate").mask('SSS-0A00', {'S': {pattern: /[a-zA-Z]/}});
  $(".mask-cep").mask('00000-000');
  $(".mask-cpf").mask('000.000.000-00', {reverse: true});
  $(".mask-cnpj").mask('00.000.000/0000-00', {reverse: true});
  $(".mask-date").mask('00/00/0000');
  $(".mask-datetime").mask('00/00/0000 00:00');
  $(".mask-money").mask('000.000.000.000.000,00', {reverse: true, placeholder: "0,00"});
  $(".mask-month").mask('00/0000', {reverse: true});

  //VALIDATIONS
  $(".mask-cpf").on("blur", function() {
    let required = $(this).attr('required');
    let soma = 0;
    let resto = 0;
    let strCPF = $(".mask-cpf").val().replace(/[^0-9]/g, '');
    $(".mask-cpf").removeClass("is-valid is-invalid");
    if(required) $("#btn-submit").attr("disabled", "disabled");

    if (strCPF == "" || strCPF == "00000000000" || strCPF == "11111111111" || strCPF == "22222222222" || strCPF == "33333333333" || strCPF == "44444444444" || strCPF == "55555555555" || strCPF == "66666666666" || strCPF == "77777777777" || strCPF == "88888888888" || strCPF == "99999999999" || strCPF == "12345678909") {
      if(required) $(".mask-cpf").addClass("is-invalid");
      return;
    }

    for (i=1; i<=9; i++) soma = soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
    resto = (soma * 10) % 11;
  
    if ((resto == 10) || (resto == 11))  resto = 0;
    if (resto != parseInt(strCPF.substring(9, 10)) ) {
      $(".mask-cpf").addClass("is-invalid");
      return;
    }
  
    soma = 0;
    for (i = 1; i <= 10; i++) soma = soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    resto = (soma * 10) % 11;

    if ((resto == 10) || (resto == 11))  resto = 0;
    if (resto != parseInt(strCPF.substring(10, 11) ) ) {
      $(".mask-cpf").addClass("is-invalid");
      return;
    }

    $(".mask-cpf").addClass("is-valid");
    $("#btn-submit").removeAttr("disabled");
    return;
  });

  $(".mask-cnpj").on("blur", function(e) {
    let required = $(this).attr('required');
    let tamanho = 0;
    let numeros = 0;
    let digitos = 0;
    let soma = 0;
    let pos = 0;
    let resultado = 0;
    let strCNPJ = $(".mask-cnpj").val().replace(/[^0-9]/g, '');
    $(".mask-cnpj").removeClass("is-valid is-invalid");
    if(required) $("#btn-submit").attr("disabled", "disabled");

    if (strCNPJ == "" || strCNPJ == "00000000000000" || strCNPJ == "11111111111111" || strCNPJ == "22222222222222" || strCNPJ == "33333333333333" || strCNPJ == "44444444444444" || strCNPJ == "55555555555555" || strCNPJ == "66666666666666" || strCNPJ == "77777777777777" || strCNPJ == "88888888888888" || strCNPJ == "99999999999999") {
      if(required) $(".mask-cnpj").addClass("is-invalid");
      return;
    }

    // Valida DVs
    tamanho = strCNPJ.length - 2
    numeros = strCNPJ.substring(0,tamanho);
    digitos = strCNPJ.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
        pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0)) {
      $(".mask-cnpj").addClass("is-invalid");
      return;
    }
         
    tamanho = tamanho + 1;
    numeros = strCNPJ.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
        pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1)) {
      $(".mask-cnpj").addClass("is-invalid");
      return;
    }
           
    $(".mask-cnpj").addClass("is-valid");
    $("#btn-submit").removeAttr("disabled");
    return;
  });
});

// AJAX RESPONSE
function ajaxMessage(message) {
  let ajaxMessage = JSON.parse(message);

  showToastr(ajaxMessage.text, ajaxMessage.type);
}

function showToastr(message, type) {
  if(type == "info") {
    toastr.info(message);
  } else if(type == "warning") {
    toastr.warning(message);
  } else if(type == "danger" || type == "error") {
    toastr.error(message);
  } else if(type == "success") {
    toastr.success(message);
  }
}