<?php $v->layout("_theme"); ?>

<?php $v->start("styles"); ?>
<link rel="stylesheet" href="<?= theme("/assets/plugins/fullcalendar/main.css", CONF_VIEW_THEME_ADMIN); ?>" />
<?php $v->end(); ?>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-10">
        <h1 class="m-0"><i class="fas fa-sm fa-calendar-alt"></i> Agenda</h1>
      </div>
      <?php if(hasPermission(["event_create"])): ?>
        <div class="col-sm-2">
          <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#add-event"><i class="fas fa-plus"></i> Incluir</button>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <?php $v->insert("_includes/messageFlash.php"); ?>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-body p-0">
            <div id="calendar"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php 
    if(hasPermission(["event_create"])) {
      $v->insert("events/add-event", [
      ]);
    }
?>
<?php $v->start("scripts"); ?>
<script src="<?= theme("/assets/plugins/fullcalendar/main.js", CONF_VIEW_THEME_ADMIN); ?>"></script>
<script src="<?= theme("/assets/plugins/fullcalendar/locales/pt-br.js", CONF_VIEW_THEME_ADMIN); ?>"></script>
<script>
  $(function () {
    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
			locale: "pt-br",
      headerToolbar: {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      themeSystem: 'bootstrap',
      events: [
        <?php if(!empty($events)): ?>
          <?php foreach($events as $event): ?>
            {
              title : "<?= ($event["description"] ?? "N/D"); ?>",
              start : new Date(<?= ($event["date"]["y"] ?? date("Y")); ?>, <?= ($event["date"]["m"] ?? date("m")) - 1; ?>, <?= ($event["date"]["d"] ?? date("d")); ?>, <?= ($event["start"]["h"] ?? date("h")); ?>, <?= ($event["start"]["m"] ?? date("i")); ?>),
              end : new Date(<?= ($event["date"]["y"] ?? date("Y")); ?>, <?= ($event["date"]["m"] ?? date("m")) -1; ?>, <?= ($event["date"]["d"] ?? date("d")); ?>, <?= ($event["end"]["h"] ?? date("h")); ?>, <?= ($event["end"]["m"] ?? date("i")); ?>),
              url: "<?= url("/events/view/{$event['id']}") ?>"
            },    
          <?php endforeach; ?>
        <?php endif; ?>
      ],
      editable  : false,
      droppable : false
    });

    calendar.render();
    // $('#calendar').fullCalendar()
  })
</script>
<?php $v->end(); ?>