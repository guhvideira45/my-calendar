<div class="modal fade" id="add-event">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Nova Agenda</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= url("/events/save"); ?>" method="post">
				<input type="hidden" name="event" value="" />
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<label>*Descrição:</label>
								<input type="text" class="form-control" name="description" placeholder="Descrição do evento" value="" required>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="form-group">
								<label>*Data:</label>
								<input type="date" class="form-control" name="date" required>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="form-group">
								<label>Valor:</label>
								<input type="text" class="form-control mask-money" name="value">
							</div>
						</div>
						<div class="col-sm-12">
							<label>*Hora Inicio:</label>
							<div class="row">
								<div class="col-sm-12 col-md-6">
									<select class="form-control" name="start_hour">
											<option value="0">00</option>
											<option value="1">01</option>
											<option value="2">02</option>
											<option value="3">03</option>
											<option value="4">04</option>
											<option value="5">05</option>
											<option value="6">06</option>
											<option value="7">07</option>
											<option value="8">08</option>
											<option value="9">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
									</select>
								</div>
								<div class="col-sm-12 col-md-6">
									<select class="form-control" name="start_minute">
											<option value="0">00</option>
											<option value="30">30</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-sm-12 p-2">
							<label>*Hora Fim:</label>
							<div class="row">
								<div class="col-sm-12 col-md-6">
									<select class="form-control" name="end_hour">
											<option value="0">00</option>
											<option value="1">01</option>
											<option value="2">02</option>
											<option value="3">03</option>
											<option value="4">04</option>
											<option value="5">05</option>
											<option value="6">06</option>
											<option value="7">07</option>
											<option value="8">08</option>
											<option value="9">09</option>
											<option value="10">10</option>
											<option value="11">11</option>
											<option value="12">12</option>
											<option value="13">13</option>
											<option value="14">14</option>
											<option value="15">15</option>
											<option value="16">16</option>
											<option value="17">17</option>
											<option value="18">18</option>
											<option value="19">19</option>
											<option value="20">20</option>
											<option value="21">21</option>
											<option value="22">22</option>
											<option value="23">23</option>
									</select>
								</div>
								<div class="col-sm-12 col-md-6">
									<select class="form-control" name="end_minute">
											<option value="0">00</option>
											<option value="30">30</option>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-between">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary"><i class="fas fa-plus-square"></i> Salvar</button>
				</div>
			</form>
		</div>
	</div>
</div>