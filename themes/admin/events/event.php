<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-8">
				<h1 class="m-0"><i class="fas fa-sm fa-calendar-alt"></i> #<?= $event->id; ?></h1>
			</div>
			<div class="col-sm-4">
				<a href="<?= url("/events"); ?>" class="btn font-weight-bold float-right" ><i class="fas fa-arrow-left"></i> Voltar</a>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<form action="<?= url("/events/save"); ?>" method="post">
						<input type="hidden" name="event" value="<?= $event->id ?? "" ?>" />
						<div class="card-body">
							<?php $v->insert("_includes/messageFlash.php"); ?>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>*Descrição:</label>
										<input type="text" class="form-control" name="description" placeholder="Descrição do evento" value="<?= ($event->description ?? "N/D"); ?>" required>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<div class="form-group">
										<label>*Data:</label>
										<input type="date" class="form-control" name="date" value="<?= ($event->date ?? "") ?>" required>
									</div>
								</div>
								<div class="col-sm-12 col-md-6">
									<div class="form-group">
										<label>Valor:</label>
										<input type="text" class="form-control mask-money" name="value" value="<?= ($event->value ?? ""); ?>">
									</div>
								</div>
								<div class="col-sm-12">
									<label>*Hora Inicio:</label>
									<div class="row">
										<div class="col-sm-12 col-md-6">
											<select class="form-control" name="start_hour">
												<option value="0" <?= ((int)explode(":", $event->start)[0] == "0" ? "selected" : ""); ?>>00</option>
												<option value="1" <?= ((int)explode(":", $event->start)[0] == "1" ? "selected" : ""); ?>>01</option>
												<option value="2" <?= ((int)explode(":", $event->start)[0] == "2" ? "selected" : ""); ?>>02</option>
												<option value="3" <?= ((int)explode(":", $event->start)[0] == "3" ? "selected" : ""); ?>>03</option>
												<option value="4" <?= ((int)explode(":", $event->start)[0] == "4" ? "selected" : ""); ?>>04</option>
												<option value="5" <?= ((int)explode(":", $event->start)[0] == "5" ? "selected" : ""); ?>>05</option>
												<option value="6" <?= ((int)explode(":", $event->start)[0] == "6" ? "selected" : ""); ?>>06</option>
												<option value="7" <?= ((int)explode(":", $event->start)[0] == "7" ? "selected" : ""); ?>>07</option>
												<option value="8" <?= ((int)explode(":", $event->start)[0] == "8" ? "selected" : ""); ?>>08</option>
												<option value="9" <?= ((int)explode(":", $event->start)[0] == "9" ? "selected" : ""); ?>>09</option>
												<option value="10" <?= ((int)explode(":", $event->start)[0] == "10" ? "selected" : ""); ?>>10</option>
												<option value="11" <?= ((int)explode(":", $event->start)[0] == "11" ? "selected" : ""); ?>>11</option>
												<option value="12" <?= ((int)explode(":", $event->start)[0] == "12" ? "selected" : ""); ?>>12</option>
												<option value="13" <?= ((int)explode(":", $event->start)[0] == "13" ? "selected" : ""); ?>>13</option>
												<option value="14" <?= ((int)explode(":", $event->start)[0] == "14" ? "selected" : ""); ?>>14</option>
												<option value="15" <?= ((int)explode(":", $event->start)[0] == "15" ? "selected" : ""); ?>>15</option>
												<option value="16" <?= ((int)explode(":", $event->start)[0] == "16" ? "selected" : ""); ?>>16</option>
												<option value="17" <?= ((int)explode(":", $event->start)[0] == "17" ? "selected" : ""); ?>>17</option>
												<option value="18" <?= ((int)explode(":", $event->start)[0] == "18" ? "selected" : ""); ?>>18</option>
												<option value="19" <?= ((int)explode(":", $event->start)[0] == "19" ? "selected" : ""); ?>>19</option>
												<option value="20" <?= ((int)explode(":", $event->start)[0] == "20" ? "selected" : ""); ?>>20</option>
												<option value="21" <?= ((int)explode(":", $event->start)[0] == "21" ? "selected" : ""); ?>>21</option>
												<option value="22" <?= ((int)explode(":", $event->start)[0] == "22" ? "selected" : ""); ?>>22</option>
												<option value="23" <?= ((int)explode(":", $event->start)[0] == "23" ? "selected" : ""); ?>>23</option>
											</select>
										</div>
										<div class="col-sm-12 col-md-6">
											<select class="form-control" name="start_minute">
												<option value="0" <?= ((int)explode(":", $event->start)[1] == "0" ? "selected" : ""); ?>>00</option>
												<option value="30" <?= ((int)explode(":", $event->start)[1] == "30" ? "selected" : ""); ?>>30</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-sm-12 p-2">
									<label>*Hora Fim:</label>
									<div class="row">
										<div class="col-sm-12 col-md-6">
											<select class="form-control" name="end_hour">
												<option value="0" <?= ((int)explode(":", $event->end)[0] == "0" ? "selected" : ""); ?>>00</option>
												<option value="1" <?= ((int)explode(":", $event->end)[0] == "1" ? "selected" : ""); ?>>01</option>
												<option value="2" <?= ((int)explode(":", $event->end)[0] == "2" ? "selected" : ""); ?>>02</option>
												<option value="3" <?= ((int)explode(":", $event->end)[0] == "3" ? "selected" : ""); ?>>03</option>
												<option value="4" <?= ((int)explode(":", $event->end)[0] == "4" ? "selected" : ""); ?>>04</option>
												<option value="5" <?= ((int)explode(":", $event->end)[0] == "5" ? "selected" : ""); ?>>05</option>
												<option value="6" <?= ((int)explode(":", $event->end)[0] == "6" ? "selected" : ""); ?>>06</option>
												<option value="7" <?= ((int)explode(":", $event->end)[0] == "7" ? "selected" : ""); ?>>07</option>
												<option value="8" <?= ((int)explode(":", $event->end)[0] == "8" ? "selected" : ""); ?>>08</option>
												<option value="9" <?= ((int)explode(":", $event->end)[0] == "9" ? "selected" : ""); ?>>09</option>
												<option value="10" <?= ((int)explode(":", $event->end)[0] == "10" ? "selected" : ""); ?>>10</option>
												<option value="11" <?= ((int)explode(":", $event->end)[0] == "11" ? "selected" : ""); ?>>11</option>
												<option value="12" <?= ((int)explode(":", $event->end)[0] == "12" ? "selected" : ""); ?>>12</option>
												<option value="13" <?= ((int)explode(":", $event->end)[0] == "13" ? "selected" : ""); ?>>13</option>
												<option value="14" <?= ((int)explode(":", $event->end)[0] == "14" ? "selected" : ""); ?>>14</option>
												<option value="15" <?= ((int)explode(":", $event->end)[0] == "15" ? "selected" : ""); ?>>15</option>
												<option value="16" <?= ((int)explode(":", $event->end)[0] == "16" ? "selected" : ""); ?>>16</option>
												<option value="17" <?= ((int)explode(":", $event->end)[0] == "17" ? "selected" : ""); ?>>17</option>
												<option value="18" <?= ((int)explode(":", $event->end)[0] == "18" ? "selected" : ""); ?>>18</option>
												<option value="19" <?= ((int)explode(":", $event->end)[0] == "19" ? "selected" : ""); ?>>19</option>
												<option value="20" <?= ((int)explode(":", $event->end)[0] == "20" ? "selected" : ""); ?>>20</option>
												<option value="21" <?= ((int)explode(":", $event->end)[0] == "21" ? "selected" : ""); ?>>21</option>
												<option value="22" <?= ((int)explode(":", $event->end)[0] == "22" ? "selected" : ""); ?>>22</option>
												<option value="23" <?= ((int)explode(":", $event->end)[0] == "23" ? "selected" : ""); ?>>23</option>
											</select>
										</div>
										<div class="col-sm-12 col-md-6">
											<select class="form-control" name="end_minute">
												<option value="0" <?= ((int)explode(":", $event->end)[1] == "0" ? "selected" : ""); ?>>00</option>
												<option value="30" <?= ((int)explode(":", $event->end)[1] == "30" ? "selected" : ""); ?>>30</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php if(hasPermission(["event_update", "event_delete"])): ?>
							<div class="card-footer">
								<?php if(hasPermission(["event_delete"])): ?>
									<a href="#" class="btn font-weight-bold text-danger" data-post="<?= url("/events/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o evento e todos os dados relacionados a ele? Essa ação não pode ser feita!" data-id="<?= $event->id; ?>"><i class="fas fa-exclamation-triangle"></i> Excluir Evento</a>
								<?php endif; ?>
								<?php if(hasPermission(["event_update"])): ?>
									<button class="btn btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Atualizar</button>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</form>
				</div>
			</div>
		</div>		
	</div>
</section>