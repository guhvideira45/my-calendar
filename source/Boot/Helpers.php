<?php

/**
 * ####################
 * ###   VALIDATE   ###
 * ####################
 */

/**
 * @param string $email
 * @return bool
 */
function is_email(string $email): bool
{
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * @param string $password
 * @return bool
 */
function is_passwd(string $password): bool
{
	if (password_get_info($password)['algo'] || (mb_strlen($password) >= CONF_PASSWD_MIN_LEN && mb_strlen($password) <= CONF_PASSWD_MAX_LEN)) {
		return true;
	}

	return false;
}

/**
 * ##################
 * ###   STRING   ###
 * ##################
 */

/**
 * @param string $string
 * @return string
 */
function str_slug(string $string): string
{
	$string = filter_var(mb_strtolower($string), FILTER_SANITIZE_STRIPPED);
	$formats = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
	$replace = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr								 ';

	$slug = str_replace(
		["-----", "----", "---", "--"],
		"-",
		str_replace(
			" ",
			"-",
			trim(strtr(utf8_decode($string), utf8_decode($formats), $replace))
		)
	);
	return $slug;
}

/**
 * @param string $string
 * @return string
 */
function str_studly_case(string $string): string
{
	$string = str_slug($string);
	$studlyCase = str_replace(
		" ",
		"",
		mb_convert_case(str_replace("-", " ", $string), MB_CASE_TITLE)
	);

	return $studlyCase;
}

/**
 * @param string $string
 * @return string
 */
function str_camel_case(string $string): string
{
	return lcfirst(str_studly_case($string));
}

/**
 * @param string $string
 * @return string
 */
function str_title(string $string): string
{
	return mb_convert_case(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS), MB_CASE_TITLE);
}

/**
 * @param string $string
 * @param int $limit
 * @param string $pointer
 * @return string
 */
function str_limit_words(string $string, int $limit, string $pointer = "..."): string
{
	$string = trim(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS));
	$arrWords = explode(" ", $string);
	$numWords = count($arrWords);

	if ($numWords <= $limit) {
		return $string;
	}

	$words = implode(" ", array_slice($arrWords, 0, $limit));
	return "{$words}{$pointer}";
}

/**
 * @param string $string
 * @param int $limit
 * @param string $pointer
 * @return string
 */
function str_limit_chars(string $string, int $limit, string $pointer = "..."): string
{
	$string = trim(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS));
	if (mb_strlen($string) <= $limit) {
		return $string;
	}

	// $chars = mb_substr($string, 0, mb_strrpos(mb_substr($string, 0, $limit), " "));
	$chars = mb_substr($string, 0, $limit);
	return "{$chars}{$pointer}";
}

/**
 * @param string $price
 * @return null|string
 */
function str_price(?string $price): string
{
	return number_format((!empty($price) ? $price : 0), 2, ",", ".");
}

/**
 * @param string $text
 * @return string
 */
function str_textarea(string $text): string
{
	$text = filter_var($text, FILTER_SANITIZE_STRIPPED);
	$arrayReplace = ["&#10;", "&#10;&#10;", "&#10;&#10;&#10;", "&#10;&#10;&#10;&#10;", "&#10;&#10;&#10;&#10;&#10;"];
	return "<p>" . str_replace($arrayReplace, "</p><p>", $text) . "</p>";
}

/**
 * @param string|null $search
 * @return string
 */
function str_search(?string $search): string
{
	if (!$search) {
		return "all";
	}

	$search = preg_replace("/[^a-z0-9A-Z\@\ ]/", "", $search);
	return (!empty($search) ? $search : "all");
}


/**
 * ###############
 * ###   URL   ###
 * ###############
 */

/**
 * @param string|null $path
 * @param string|CONF_URL_BASE $urlBase
 * @return string
 */
function url(string $path = null, string $urlBase = CONF_URL_BASE): string
{
	if (strpos($_SERVER['HTTP_HOST'], 'localhost') || strpos($_SERVER['HTTP_HOST'], '127.0.0.1'))
		$urlBase = CONF_URL_DEV;

	if ($path)
		return $urlBase . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);

	return $urlBase;
}

/**
 * @return string
 */
function url_back(): string
{
	return ($_SERVER['HTTP_REFERER'] ?? url());
}

/**
 * @param string $url
 * @return void
 */
function redirect(string $url): void
{
	header("HTTP/1.1 302 Redirect");
	if (filter_var($url, FILTER_VALIDATE_URL)) {
		header("Location: {$url}");
		exit;
	}

	if (filter_input(INPUT_GET, "route", FILTER_DEFAULT) != $url) {
		$location = url($url);
		header("Location: {$location}");
		exit;
	}
}


/**
 * ##################
 * ###   ASSETS   ###
 * ##################
 */

/**
 * @return \Source\Models\User|null
 */
function user(): ?\Source\Models\User
{
	return \Source\Models\Auth::user();
}

/**
 * @return \Source\Core\Session
 */
function session(): \Source\Core\Session
{
	return new \Source\Core\Session();
}

/**
 * @param string|null $path
 * @param string|CONF_VIEW_THEME_ADMIN $theme
 * @param string|CONF_URL_BASE $urlBase
 * @return string
 */
function theme(string $path = null, string $theme = CONF_VIEW_THEME_ADMIN, string $urlBase = CONF_URL_BASE): string
{
	if (strpos($_SERVER['HTTP_HOST'], 'localhost') || strpos($_SERVER['HTTP_HOST'], '127.0.0.1'))
		$urlBase = CONF_URL_DEV;

	$urlBase = $urlBase . "/themes/{$theme}";

	if ($path)
		return $urlBase . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);

	return $urlBase;
}

/**
 * @param null|string $image
 * @param int $width
 * @param int $height
 * @return null|string
 */
function image(?string $image, int $width, int $height = null): ?string
{
	if ($image) {
		return url() . "/" . (new \Source\Support\Thumb())->make($image, $width, $height);
	}

	return null;
}

/**
 * ################
 * ###   DATE   ###
 * ################
 */

/**
 * @param null|string $date
 * @param string $format
 * @return string
 */
function date_fmt(?string $date, string $format = "d/m/Y H\hi"): string
{
	$date = (!empty($date) ? $date : "now");
	return (new DateTime($date))->format($format);
}

/**
 * @param null|string $date
 * @return string
 */
function date_fmt_br(?string $date): string
{
	$date = (!empty($date) ? $date : "now");
	return (new DateTime($date))->format(CONF_DATE_BR);
}

/**
 * @param null|string $date
 * @return string
 */
function date_fmt_app(?string $date): string
{
	$date = (!empty($date) ? $date : "now");
	return (new DateTime($date))->format(CONF_DATE_APP);
}

function date_fmt_back(?string $date): ?string
{
	if (!$date)
		return null;

	if (strpos($date, " ")) {
		$date = explode(" ", $date);
		return implode("-", array_reverse(explode("/", $date[0]))) . " " . $date[1];
	}

	return implode("-", array_reverse(explode("/", $date)));
}

/**
 * ####################
 * ###   PASSWORD   ###
 * ####################
 */

/**
 * @param string $password
 * @return string
 */
function passwd(string $password): string
{
	if (!empty(password_get_info($password)['algo']))
		return $password;

	return password_hash($password, CONF_PASSWD_ALGO, CONF_PASSWD_OPTION);
}

/**
 * @param string $password
 * @param string $hash
 * @return bool
 */
function passwd_verify(string $password, string $hash): bool
{
	return password_verify($password, $hash);
}

/**
 * @param string $hash
 * @return bool
 */
function passwd_rehash(string $hash): bool
{
	return password_needs_rehash($hash, CONF_PASSWD_ALGO, CONF_PASSWD_OPTION);
}

/**
 * ######################
 * ###   PERMISSION   ###
 * ######################
 */

/**
 * @param string|array $password
 * @return bool
 */
function hasPermission($permissions): bool
{
	$userAdmin = session()->userAdmin;
	$userPermissions = (array)session()->userPermissions;

	if ($userAdmin || user()->id == 1) {
		return true;
	}

	if(is_array($permissions)) {
		foreach($permissions as $permission) {
			if(in_array($permission, $userPermissions)) {
				return true;
			}
		}
	} else {
		if(in_array($permissions, $userPermissions)) {
			return true;
		}
	}	

	return false;
}

/**
 * @var string|array $permissions
 * @var bool $isJson
 */
function checkPermission($permissions, bool $isJson = false)
{
	if(!hasPermission($permissions)) {
		if($isJson) {
			$json["redirect"] = url("/ooops/403");
			echo json_encode($json);
			die();
		} else {
			redirect("/ooops/403");
		}
	}
}

/**
 * ###################
 * ###   REQUEST   ###
 * ###################
 */

/**
 * @return string
 */
function csrf_input(): string
{
	$session = new \Source\Core\Session();
	$session->csrf();
	return "<input type='hidden' name='csrf' value='" . ($session->csrf_token ?? "") . "'/>";
}

/**
 * @param $request
 * @return bool
 */
function csrf_verify($request): bool
{
	$session = new \Source\Core\Session();
	if (empty($session->csrf_token) || empty($request['csrf']) || $request['csrf'] != $session->csrf_token) {
		return false;
	}
	return true;
}

/**
 * @return null|string
 */
function flash(): ?string
{
	$session = new \Source\Core\Session();
	if ($flash = $session->flash())
		echo $flash;

	return null;
}

/**
 * @param string $key
 * @param int $limit
 * @param int $seconds
 * @return bool
 */
function request_limit(string $key, int $limit = 5, int $seconds = 60): bool
{
	$session = new \Source\Core\Session();
	if ($session->has($key) && $session->$key->time >= time() && $session->$key->requests < $limit) {
		$session->set($key, [
			"time" => time() + $seconds,
			"requests" => $session->$key->requests + 1
		]);
		return false;
	}

	if ($session->has($key) && $session->$key->time >= time() && $session->$key->requests >= $limit) {
		return true;
	}

	$session->set($key, [
		"time" => time() + $seconds,
		"requests" => 1
	]);
	return false;
}

/**
 * @param string $field
 * @param string $value
 * @return bool
 */
function request_repeat(string $field, string $value): bool
{
	$session = new \Source\Core\Session();
	if ($session->has($field) && $session->$field == $value) {
		return true;
	}

	$session->set($field, $value);
	return false;
}