<?php
if(!strpos(url(), "localhost") || strpos(url(), "127.0.0.1")) {
	/**
	 * CSS
	 */
	$minCSS = new \MatthiasMullie\Minify\CSS();
	$minCSS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/fontawesome-free/css/all.min.css");
	$minCSS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/select2/css/select2.min.css");
	$minCSS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/daterangepicker/daterangepicker.css");
	$minCSS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css");
	$minCSS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/toastr/toastr.min.css");
	$minCSS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/css/adminlte.min.css");
	$minCSS->add(__DIR__ . "/../../../shared/styles/boot.css");

	// THEME CSS
	// $cssDir = scandir(__DIR__."/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/css");
	// foreach($cssDir as $css) {
	// 	$cssFile = __DIR__."/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/css/{$css}";
	// 	if(is_file($cssFile) && pathinfo($cssFile)['extension'] == "css") {
	// 		$minCSS->add($cssFile);
	// 	}
	// }

	// Minify CSS
	$minCSS->minify(__DIR__."/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/style.css");

	/**
	 * JS
	 */
	$minJS = new \MatthiasMullie\Minify\JS();
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/jquery/jquery.min.js");
	$minJS->add(__DIR__ . "/../../../shared/scripts/jquery.form.js");
	$minJS->add(__DIR__ . "/../../../shared/scripts/jquery-ui.js");
	$minJS->add(__DIR__ . "/../../../shared/scripts/jquery.mask.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/bootstrap/js/bootstrap.bundle.min.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/toastr/toastr.min.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/select2/js/select2.full.min.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/daterangepicker/daterangepicker.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/js/adminlte.min.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/plugins/moment/moment.min.js");
	$minJS->add(__DIR__ . "/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/js/scripts.js");

	// THEME JS
	// $jsDir = scandir(__DIR__."/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/js");
	// foreach($jsDir as $js) {
	// 	$jsFile = __DIR__."/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/js/{$js}";
	// 	if(is_file($jsFile) && pathinfo($jsFile)['extension'] == "js") {
	// 		$minJS->add($jsFile);
	// 	}
	// }

	// Minify JS
	$minJS->minify(__DIR__."/../../../themes/".CONF_VIEW_THEME_ADMIN."/assets/scripts.js");

}