<?php

namespace Source\App;

use \stdClass;
use Source\Core\Controller;

/**
 * Error controller
 * @package Source\App
 */
class Error extends Controller
{
	/**
	 * Error constructor
	 */
	public function __construct()
	{
		parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME_ADMIN . "/");
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function error(array $data): void
	{
		$error = new stdClass;

		switch ($data['errcode']) {
			case 'problemas':
				$error->code = "Ooops";
				$error->title = "Estamos enfrentando problemas!";
				$error->message = "Parece que nosso serviço não está disponível no momento. Já estamos vendo isso mas caso precise, envie-nos um e-mail :)";
				$error->linkTitle = "Enviar E-mail";
				$error->link = "mailto:" . CONF_MAIL_SUPPORT;
				break;

			case 'manutencao':
				$error->code = "Ooops";
				$error->title = "Desculpe. Estemos em manutenção!";
				$error->message = "Voltamos logo! Por hora estamos trabalhando para melhorar nosso conteúdo para você :P";
				$error->linkTitle = null;
				$error->link = null;
				break;

			default:
				$error->code = $data['errcode'];
				$error->title = "Ooops conteúdo indisponivel :/";
				$error->message = "Sentimos muito, mas o conteúdo que você tentou acessar não existe, está indisponível no momento ou foi removido :/";
				$error->linkTitle = "Continue navegando";
				$error->link = url_back();
				break;
		}

		$head = $this->seo->render(
			"{$error->code} | {$error->title}",
			$error->message,
			url("/ooops/{$error->code}"),
			theme("/assets/images/share.jpg"),
			false
		);

		echo $this->view->render("error", [
			"head" => $head,
			"error" => $error
		]);
	}
}