<?php

namespace Source\App;

use Source\Models\Profile;
use Source\Models\User;
use Source\Support\Pager;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Profiles extends Admin
{
	/** @var string */
	private $app;
	
	/** @var string */
	private $group;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->app = "profiles";
		$this->group = "settings";
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function home(?array $data): void
	{
		checkPermission(["profile_view"]);

		//SEARCH REDIRECT
		if (!empty($data["s"])) {
			$s = str_search($data["s"]);
			$json["redirect"] = url("/users/{$s}/1");
			echo json_encode($json);
			return;
		}

		$search = null;
		$profiles = (new Profile())->find("deleted_at IS NULL");

		if (!empty($data["search"]) && str_search($data["search"]) != "all") {
			$search = str_search($data["search"]);
			$profiles = (new Profile())->find("MATCH(name) AGAINST(:s) AND deleted_at IS NULL", "s={$search}");
			if (!$profiles->count()) {
				$this->message->info("Sua pesquisa não retornou resultados")->flash();
				redirect("/profiles");
			}
		}

		$all = ($search ?? "all");
		$pager = new Pager(url("/profiles/{$all}/"));
		$pager->pager($profiles->count(), 9, (!empty($data["page"]) ? $data["page"] : 1));

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Perfis de acesso",
			CONF_SITE_DESC,
			url("/"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("profiles/index", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"search" => $search,
			"profiles" => $profiles->limit($pager->limit())->offset($pager->offset())->fetch(true),
			"paginator" => $pager->render()
		]);
	}

	/**
	 * @return void
	 */
	public function create(): void
	{
		checkPermission(["profile_create"]);

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Novo perfil",
			CONF_SITE_DESC,
			url("/admin"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("profiles/profile", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"profile" => null
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function view(array $data): void
	{
		checkPermission(["profile_view"]);

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$profile = (new Profile())->findById($id);
		if (!$profile) {
			$this->message->error("Você tentou gerenciar um cadastro que não existe")->flash();
			redirect("/profiles");
		}

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Perfil de {$profile->name}",
			CONF_SITE_DESC,
			url("/admin"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("profiles/profile", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"profile" => $profile
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function save(array $data): void
	{
		$data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

		$profile = new Profile();
		if (!empty($id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT))) {
			checkPermission(["profile_create"]);

			$profile = (new Profile())->findById($id);
			if (!$profile) {
				$json["message"] = $this->message->error("Você tentou gerenciar um cadastro que não existe")->toastr();
				echo json_encode($json);
				return;
			}
		} else {
			checkPermission(["profile_update"]);
		}

		$profile->name = ($data["name"] ?? null);
		$profile->permissions = (($data["admin"] ?? "false") == "true" ? "admin" : (!empty($data["permissions"]) ? (is_array($data["permissions"]) ? implode(";", $data["permissions"]) : $data["permissions"]) : null));
		$profile->status = (!empty($data["status"]) && in_array($data["status"], ["active", "inactive"]) ? $data["status"] : "inactive");

		if (!$profile->save()) {
			$json["message"] = $profile->message()->toastr();
			echo json_encode($json);
			return;
		}

		if (!empty($id)) {
			$this->message->success("Cadastro atualizado com sucesso.")->flash();
			$json["reload"] = true;
		} else {
			$this->message->success("Cadastro incluido com sucesso. Confira...")->flash();
			$json["redirect"] = url("/profiles/view/{$profile->id}");
		}
		
		echo json_encode($json);
		return;
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function delete(array $data): void
	{
		checkPermission(["profile_delete"]);

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$profile = (new Profile())->findById($id);
		if (!$profile) {
			$json["message"] = $this->message->error("Você tentou gerenciar um cadastro que não existe")->toastr();
			echo json_encode($json);
			return;
		}

		$userCount = (new User())->find("profile_id = :profile AND deleted_at IS NULL", "profile={$profile->id}", "COUNT(*) AS value")->fetch();
		if($userCount->value > 0) {
			$json["message"] = $this->message->warning("Você tentou excluir um perfil que possui usuários atrelados!")->toastr();
			echo json_encode($json);
			return;
		}

		$profile->destroy();
		$this->message->success("Cadastro excluido com sucesso...")->flash();
		$json["redirect"] = url("/profiles");
		echo json_encode($json);
		return;
	}
}