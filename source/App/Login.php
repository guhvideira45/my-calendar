<?php

namespace Source\App;

use Source\Core\Controller;
use Source\Models\Auth;

/**
 * Class Login
 * @package Source\App\Admin
 */
class Login extends Controller
{
	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME_ADMIN . "/");
	}

	/**
	 * Admin access redirect
	 * @return void
	 */
	public function root(): void
	{
		$user = user();
		if ($user && $user->status == 'active') {
			redirect("/events");
		} else {
			redirect("/login");
		}
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function login(?array $data): void
	{
		$user = user();
		if ($user && $user->status == 'active') {
			redirect("/events");
		}

		//LOGIN
		if (!empty($data["email"]) && !empty($data["password"])) {
			if (!csrf_verify($data)) {
				$json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
				echo json_encode($json);
				return;
			}

			if (request_limit("loginlogin", 3, 5 * 60)) {
				$json["message"] = $this->message->error("ACESSO NEGADO: Aguarde por 5 minutos para tentar novamente.")->render();
				echo json_encode($json);
				return;
			}

			$auth = new Auth();
			$login = $auth->login($data["email"], $data["password"], true);
			if ($login) {
				$json["redirect"] = url("/events");
			} else {
				$json["message"] = $auth->message()->render();
			}

			echo json_encode($json);
			return;
		}

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Login",
			CONF_SITE_DESC,
			url("/"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("auth/login", [
			"head" => $head
		]);
	}

	/**
	 * @return void
	 */
	public function logoff(): void
	{
		$this->message->success("Você saiu com sucesso ". user()->first_name)->flash();

		Auth::logout();
		redirect("/login");
	}
}