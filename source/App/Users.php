<?php

namespace Source\App;

use Source\Models\User;
use Source\Models\Profile;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Users extends Admin
{
	/** @var string */
	private $app;

	/** @var string */
	private $group;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->app = "users";
		$this->group = "settings";
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function home(?array $data): void
	{
		checkPermission(["user_view"]);

		//SEARCH REDIRECT
		if (!empty($data["s"])) {
			$s = str_search($data["s"]);
			$json["redirect"] = url("/users/{$s}/1");
			echo json_encode($json);
			return;
		}

		$search = null;
		$users = (new User())->find("id > 1 AND deleted_at IS NULL");

		if (!empty($data["search"]) && str_search($data["search"]) != "all") {
			$search = str_search($data["search"]);
			$users = (new User())->find("id > 1 AND MATCH(first_name, last_name, email) AGAINST(:s) AND deleted_at IS NULL", "s={$search}");
			if (!$users->count()) {
				$this->message->info("Sua pesquisa não retornou resultados")->flash();
				redirect("/users");
			}
		}

		$all = ($search ?? "all");
		$pager = new Pager(url("/users/{$all}/"));
		$pager->pager($users->count(), 9, (!empty($data["page"]) ? $data["page"] : 1), 2);

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Usuários",
			CONF_SITE_DESC,
			url("/"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("users/index", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"search" => $search,
			"users" => $users->order("first_name, last_name")->limit($pager->limit())->offset($pager->offset())->fetch(true),
			"paginator" => $pager->render()
		]);
	}

	/**
	 * @return void
	 */
	public function create(): void
	{
		checkPermission(["user_create"]);
		
		$head = $this->seo->render(
			CONF_SITE_NAME . " | Novo Usuário",
			CONF_SITE_DESC,
			url("/admin"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("users/user", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"user" => null,
			"profiles" => (new Profile())->find("status = 'active' AND deleted_at IS NULL")->fetch(true)
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function view(array $data): void
	{
		checkPermission(["user_view"]);

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$user = (new User())->findById($id);
		if (!$user || $user->id == 1) {
			$this->message->error("Você tentou gerenciar um cadastro que não existe")->flash();
			redirect("/users");
		}

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Perfil de {$user->first_name}",
			CONF_SITE_DESC,
			url("/admin"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("users/user", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"user" => $user,
			"profiles" => (new Profile())->find("status = 'active' AND deleted_at IS NULL")->fetch(true)
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function save(array $data): void
	{
		$data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

		$user = new User();
		if (!empty($id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT))) {
			checkPermission(["user_update"]);

			$user = (new User())->findById($id);
			if (!$user || $user->id == 1) {
				$json["message"] = $this->message->error("Você tentou gerenciar um cadastro que não existe")->toastr();
				echo json_encode($json);
				return;
			}
		} else {
			checkPermission(["user_create"]);
		}
		
		$user->first_name = $data["first_name"];
		$user->last_name = $data["last_name"];
		$user->email = $data["email"];
		$user->password = (!empty($data["password"]) ? $data["password"] : (!empty($id) ? $user->password : null));
		$user->profile_id = filter_var(($data["profile"] ?? 0), FILTER_VALIDATE_INT);
		$user->datebirth = (!empty($data["datebirth"]) ? date_fmt_back($data["datebirth"]) : null);
		$user->document = preg_replace("/[^0-9]/", "", $data["document"]);
		$user->status = (!empty($data["status"]) && in_array($data["status"], ["active", "inactive"]) ? $data["status"] : "inactive");

		//UPLOAD PHOTO
		if (!empty($id)) {
			if (!empty($_FILES["photo"]) && $user->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$user->photo}")) {
				unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$user->photo}");
				(new Thumb())->flush($user->photo);
			}
		}

		if (!empty($_FILES["photo"])) {
			$files = $_FILES["photo"];
			$upload = new Upload();
			$image = $upload->image($files, "user-avatar-id-{$user->id}", 600);

			if (!$image) {
				$json["message"] = $upload->message()->render();
				echo json_encode($json);
				return;
			}

			$user->photo = $image;
		}

		if (!$user->save()) {
			$json["message"] = $user->message()->toastr();
			echo json_encode($json);
			return;
		}

		if (!empty($id)) {
			$this->message->success("Cadastro atualizado com sucesso.")->flash();
			$json["reload"] = true;
		} else {
			$this->message->success("Cadastro incluido com sucesso. Confira...")->flash();
			$json["redirect"] = url("/users/view/{$user->id}");
		}
		
		echo json_encode($json);
		return;
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function delete(array $data): void
	{
		checkPermission(["user_delete"]);

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$user = (new User())->findById($id);
		if (!$user || $user->id == 1) {
			$json["message"] = $this->message->error("Você tentou gerenciar um cadastro que não existe")->toastr();
			echo json_encode($json);
			return;
		}

		if ($user->photo && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$user->photo}")) {
			unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$user->photo}");
			(new Thumb())->flush($user->photo);
		}

		$user->destroy();
		$this->message->success("Cadastro excluido com sucesso...")->flash();
		$json["redirect"] = url("/users");
		echo json_encode($json);
		return;
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function password(array $data): void
	{
		$user = user();

		if (!empty($data["action"]) && $data["action"] == "password") {
			if (empty($data["password"]) || empty($data["password_confirm"])) {
				$json["message"] = $this->message->warning("Por favor informe todos os campos")->toastr();
				echo json_encode($json);
				return;
			}

			if ($data["password"] != $data["password_confirm"]) {
				$json["message"] = $this->message->error("Os campos informados não conferem")->toastr();
				echo json_encode($json);
				return;
			}

			if (!is_passwd($data["password"])) {
				$json["message"] = $this->message->error("A senha informada não atende os requisitos minimos de segurança")->toastr();
				echo json_encode($json);
				return;
			}

			$user->password = $data["password"];
			if (!$user->save()) {
				$json["message"] = $user->message()->toastr();
				echo json_encode($json);
				return;
			}

			$this->message->success("Senha atualizada com sucesso.")->flash();
			$json["reload"] = true;
			echo json_encode($json);
			return;
		}

		$head = $this->seo->render(
			CONF_SITE_NAME . " | Alterar senha de {$user->first_name}",
			CONF_SITE_DESC,
			url("/admin"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("users/password", [
			"head" => $head,
			"user" => $user
		]);
	}
}