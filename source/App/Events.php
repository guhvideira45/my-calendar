<?php

namespace Source\App;

use Source\Models\Event;

/**
 * Class Events
 * @package Source\App\Admin
 */
class Events extends Admin
{
	/** @var string */
	private $app;

	/** @var string */
	private $group;

	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct();

		$this->app = "events";
		$this->group = null;
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function home(?array $data): void
	{
		$head = $this->seo->render(
			CONF_SITE_NAME . " | Agenda",
			CONF_SITE_DESC,
			url("/"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		$events = (new Event())->find("date >= :date AND deleted_at IS NULL", "date=".date("Y-m-d", strtotime("-1year")))->limit(4000)->fetch(true);
		$eventList = [];
		if(!empty($events)) {
			foreach ($events as $event) {
				$eventList[] = [
					"id" => $event->id,
					"description" => $event->description,
					"date" => [
						"y" => date("Y", strtotime($event->date)),
						"m" => date("m", strtotime($event->date)),
						"d" => date("d", strtotime($event->date))
					],
					"start" => [
						"h" => (int)explode(":", $event->start)[0],
						"m" => (int)explode(":", $event->start)[1],
					],
					"end" => [
						"h" => (int)explode(":", $event->end)[0],
						"m" => (int)explode(":", $event->end)[1],
					],
					"allDay" => ($event->allDay == 1 ? true : false)
				];
			}
		}

		echo $this->view->render("events/index", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"events" => $eventList
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function view(array $data): void
	{
		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$event = (new Event())->findById($id);
		if (!$event) {
			$this->message->error("Você tentou gerenciar um evento que não existe")->flash();
			redirect("/events");
		}

		$head = $this->seo->render(
			CONF_SITE_NAME . " | #{$event->id}",
			CONF_SITE_DESC,
			url("/admin"),
			theme("/assets/images/image.jpg", CONF_VIEW_THEME_ADMIN),
			false
		);

		echo $this->view->render("events/event", [
			"app" => $this->app,
			"group" => $this->group,
			"head" => $head,
			"event" => $event
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function save(array $data): void
	{
		$data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

		$event = new Event();
		if (!empty($id = filter_var(($data["event"] ?? 0), FILTER_VALIDATE_INT))) {
			checkPermission(["event_update"]);

			$event = (new Event())->findById($id);
			if (!$event) {
				$json["message"] = $this->message->error("Você tentou gerenciar um evento que não existe")->toastr();
				echo json_encode($json);
				return;
			}
		} else {
			checkPermission(["event_create"]);
		}

		$start = null;
		$end = null;
		if(!empty($data["start_hour"]) && !empty($data["end_hour"])) {
			if(($data["start_hour"] >= 0 && $data["start_hour"] <= 23) && ($data["end_hour"] >= 0 && $data["end_hour"] <= 23)) {
				$data["start_minute"] = (in_array($data["start_minute"], ["0", "30"]) ? $data["start_minute"] : "0");
				$data["end_minute"] = (in_array($data["end_minute"], ["0", "30"]) ? $data["end_minute"] : "0");
				
				if((trim($data["start_hour"]).":".$data["start_minute"]) != (trim($data["end_hour"]).":".$data["end_minute"])) {
					if($data["start_hour"] == $data["end_hour"]) {
						if($data["start_minute"] < $data["end_minute"]) {
							$start = str_pad($data["start_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["start_minute"], 2, "0", STR_PAD_LEFT);
							$end = str_pad($data["end_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["end_minute"], 2, "0", STR_PAD_LEFT);
						} else {
							$start = str_pad($data["end_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["end_minute"], 2, "0", STR_PAD_LEFT);
							$end = str_pad($data["start_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["start_minute"], 2, "0", STR_PAD_LEFT);
						}
					} elseif($data["start_hour"] > $data["end_hour"]) {
						$start = str_pad($data["end_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["end_minute"], 2, "0", STR_PAD_LEFT);
						$end = str_pad($data["start_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["start_minute"], 2, "0", STR_PAD_LEFT);
					} else {						
						$start = str_pad($data["start_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["start_minute"], 2, "0", STR_PAD_LEFT);
						$end = str_pad($data["end_hour"], 2, "0", STR_PAD_LEFT).":".str_pad($data["end_minute"], 2, "0", STR_PAD_LEFT);
					}
				}
			}
		}
		
		
		$event->description = $data["description"];
		$event->date = (!empty($data["date"]) ? date("Y-m-d", strtotime($data["date"])) : date("Y-m-d"));
		$event->value = (!empty($data["value"]) ? str_replace(",", ".", str_replace(".", "", $data["value"])) : null);
		$event->start = $start;
		$event->end = $end;
		$event->allDay = 0;

		
		if (!$event->save()) {
			$json["message"] = $event->message()->toastr();
			echo json_encode($json);
			return;
		}
		
		$this->message->success("Evento " . (!empty($id) ? "atualizado" : "incluido") . " com sucesso.")->flash();
		$json["reload"] = true;
		echo json_encode($json);
		return;
	}

	// /**
	//  * @param array $data
	//  * @return void
	//  */
	public function delete(array $data): void
	{
		checkPermission(["event_delete"]);

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$event = (new Event())->findById($id);
		if (!$event) {
			$json["message"] = $this->message->error("Você tentou gerenciar um evento que não existe")->toastr();
			echo json_encode($json);
			return;
		}

		$event->destroy();
		$this->message->success("Evento excluido com sucesso...")->flash();
		$json["redirect"] = url("/events");
		echo json_encode($json);
		return;
	}
}