<?php

namespace Source\App;

use Source\Core\Controller;
use Source\Models\Auth;
use Source\Core\Session;
use Source\Models\Invoice;

/**
 * Class Admin
 * @package Source\App\Admin
 */
class Admin extends Controller
{
	/** @var \Source\Model\User|null */
	private $user;

	/** @var string */
	private $urlRedirectPermission;

	/**
	 * Admin constructor.
	 */
	public function __construct()
	{
		parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME_ADMIN . "/");
		$session = new Session();

		$this->user = user();
		if (!$this->user) {
			$this->message->error("Para acessar é preciso logar-se")->flash();
			redirect("/login");
		}

		if(strtotime("now") > ($_SESSION["authExpire"] ?? 0)) {
			Auth::logout();
			$this->message->error("Sua sessão expirou, realize um novo login para continuar acessando")->flash();
			redirect("/login");
		}

		$session->set("authExpire", strtotime("now") + (CONF_AUTH_EXPIRE * 60));

		$this->urlRedirectPermission = "/ooops/403";
	}
}