<?php

namespace Source\Models;

use Source\Core\Model;
use Source\Models\User;

/**
 * @package Source\Models
 */
class Profile extends Model
{
  /**
   * Profile constructor.
   */
  public function __construct()
  {
    parent::__construct("profiles", ["id"], ["name", "status"]);
  }

  /**
   * @return int
   */
  public function userCount(): int
  {
    if(!empty($this->id)) {
      return (new User())->find("profile_id = {$this->id} AND deleted_at IS NULL")->count();
    }

    return 0;
  }
}