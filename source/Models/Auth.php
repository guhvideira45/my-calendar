<?php

namespace Source\Models;

use Source\Core\Model;
use Source\Core\Session;
use Source\Core\View;
use Source\Models\User;
use Source\Support\Email;

class Auth extends Model
{
	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		parent::__construct("users", ["id"], ["email", "password"]);
	}

	/**
	 * @return null|User
	 */
	public static function user(): ?User
	{
		$session = new Session();
		if (!$session->has("authUser")) {
			return null;
		}

		return (new User())->find("id = {$session->authUser} AND status = 'active' AND deleted_at IS NULL")->fetch();
	}

	/**
	 * @return void
	 */
	public static function logout(): void
	{
		$session = new Session();
		$session->unset("authUser");
		$session->unset("authExpire");
		$session->unset("userAdmin");
		$session->unset("userPermissions");
		$session->unset("loginlogin");
	}

	/**
	 * @param string $email
	 * @param string $password
	 * @param bool $save
	 * @param int $level
	 * @return bool
	 */
	public function login(string $email, string $password, bool $save = false): bool
	{
		//VERIFY
		if (!is_email($email)) {
			$this->message->warning("O e-mail informado não é válido");
			return false;
		}

		if ($save)
			setcookie("authEmail", $email, time() + 604800, "/");
		else
			setcookie("authEmail", null, time() - 3600, "/");

		if (!is_passwd($password)) {
			$this->message->warning("A senha informada não é válida");
			return false;
		}

		$user = (new User())->findByEmail($email);
		if (!$user) {
			$this->message->error("O e-mail informado não está cadastrado");
			return false;
		}

		if (!passwd_verify($password, $user->password)) {
			$this->message->error("A senha informada não confere");
			return false;
		}

		if (passwd_rehash($user->password)) {
			$user->password = $password;
			$user->save();
		}

		//LOGIN
		(new Session())->set("authUser", $user->id);
		(new Session())->set("authExpire", time() + (CONF_AUTH_EXPIRE * 60));
		(new Session())->set("userAdmin", ($user->profile()->permissions == "admin" ? true : false));
		(new Session())->set("userPermissions", explode(";", $user->profile()->permissions));
		$this->message->success("Bem vindo(a) de volta {$user->first_name}!")->flash();
		return true;
	}

	/**
	 * @param string $email
	 * @return boolean
	 */
	public function forget(string $email): bool
	{
		$user = (new User())->findByEmail($email);
		if (!$user) {
			$this->message->warning("O e-mail informado não está cadastrado");
			return false;
		}

		$user->forget = md5(uniqid(rand(), true));
		$user->save();

		$view = new View(__DIR__ . "/../../shared/views/email");
		$message = $view->render("forget", [
			"first_name" => $user->first_name,
			"forget_link" => url("/recuperar/" . base64_encode($user->email) . "|{$user->forget}")
		]);

		(new Email())->bootstrap(
			"Recupere sua senha no " . CONF_SITE_NAME,
			$message,
			$user->email,
			"{$user->first_name} {$user->last_name}"
		)->send();

		return true;
	}

	/**
	 * @param string $email
	 * @param string $code
	 * @param string $password
	 * @param string $passwordConfirm
	 * @return boolean
	 */
	public function reset(string $email, string $code, string $password, string $passwordConfirm): bool
	{
		$user = (new User())->findByEmail($email);
		if (!$user) {
			$this->message->warning("A conta para recuperação não foi encontrada");
			return false;
		}

		if ($user->forget != $code) {
			$this->message->error("Desculpe, mas o link de redefinição não é válido");
			return false;
		}

		if (!is_passwd($password)) {
			$min = CONF_PASSWD_MIN_LEN;
			$max = CONF_PASSWD_MAX_LEN;
			$this->message->info("Sua senha deve ter entre {$min} e {$max} caracteres");
			return false;
		}

		if ($password != $passwordConfirm) {
			$this->message->warning("Você informou duas senhas diferentes");
			return false;
		}

		$user->password = $password;
		$user->forget = null;
		$user->save();
		return true;
	}
}